# sccdnslib - provide python interface to KIT-SCC's WebAPI
# Copyright (C) 2017  Florian Rinke
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests
from posixpath import join as urljoin
import json
import ipaddress

import logging
logger = logging.getLogger(__name__)

BASEURL = 'https://www-net.scc.kit.edu/api/'
DEFAULTVER = "2.0"


def _query_raw(cert, path, params=None):
    logger.debug("Send GET '%s', params(%s)", path, params)
    response = requests.get(url=path, cert=cert, params=params)
    if response.ok:
        return response.json()
    else:
        raise requests.exceptions.RequestException(response.status_code, response.text)


def _post_raw(cert, path, data=None):
    logger.debug("Send POST '%s', params(%s)", path, data)
    headers_dict = {"Content-Type": "application/json"}
    response = requests.post(url=path, cert=cert, data=data, headers=headers_dict)
    if response.ok:
        return response.json()
    else:
        raise requests.exceptions.RequestException(response.status_code, response.text)


def _query_name(cert, *args, params=None):
    logger.debug("Construct get path from name '%s', params(%s)", args, params)
    url = urljoin(BASEURL, *args)
    return _query_raw(cert, url, params)


def _post_name(cert, *args, data=None):
    logger.debug("Construct post path from name '%s', params(%s)", args, data)
    url = urljoin(BASEURL, *args)
    return _post_raw(cert, url, data)


def _query(cert, major, minor, system, objtype, funcname):
    logger.debug("Construct get path from 'major=%s, minor=%s, system=%s, objtype=%s, funcname=%s'",
                 major, minor, system, objtype, funcname)
    url = urljoin(BASEURL, "{}.{}".format(major, minor), system, objtype, funcname)
    return _query_raw(cert, url)


def get_raw(cert, path):
    logger.info("Forward raw query path '%s", path)
    data = _query_raw(cert, path)
    return data


def get_name(cert, *args):
    logger.info("Forward raw query by name '%s'", args)
    data = _query_name(cert, *args)
    return data


def get_versions_num(cert):
    logger.info("Call get_versions_num")
    data = _query_name(cert)['VersionNumIndex']
    return data


def get_versions_name(cert):
    logger.info("Call get_versions_name")
    data = _query_name(cert)['VersionNameIndex']
    return data


def get_systems(cert, ver=DEFAULTVER):
    logger.info("Call get_systems")
    data = _query_name(cert, ver)['SystemIndex']
    return data


def dns_ipaddr_list(cert, params=None, ver=DEFAULTVER):
    logger.info("Call dns_ipaddr_list v%s", ver)
    data = _query_name(cert, ver, "dns", "ipaddr", "list", params=params)
    return data


def dns_mgr_list(cert, params=None, ver=DEFAULTVER):
    logger.info("Call dns_mgr_list v%s", ver)
    data = _query_name(cert, ver, "dns", "mgr", "list", params=params)
    return data


def dns_range_list(cert, params=None, ver=DEFAULTVER):
    logger.info("Call dns_range_list v%s", ver)
    data = _query_name(cert, ver, "dns", "range", "list", params=params)
    return data


def dns_record_api(cert, ver=DEFAULTVER):
    logger.info("Call dns_record_api v%s", ver)
    data = _query_name(cert, ver, "dns", "record")
    return data


def dns_record_list(cert, params=None, ver=DEFAULTVER):
    logger.info("Call dns_record_list v%s", ver)
    data = _query_name(cert, ver, "dns", "record", "list", params=params)
    return data


def _construct_inttype(address):
    logger.debug("Helper: Build inttype for address %s", address)
    dbnt_lname = 'dflt:0100'
    dbnt_rname = ':'
    # address_type = '0'
    l_uniqueness = '0'
    r_uniqueness = '2'
    address_type_dict = {
        4: 'A',
        6: 'AAAA'
    }
    # e.g. 'host:0100,:,602,AAAA'
    dbrt_mask = '{lnt},{rnt},{at}{lunq}{runq},{rrt}'

    ipaddr_obj = ipaddress.ip_address(address)
    address_type = ipaddr_obj.version
    rr_dbrt = dbrt_mask.format(
        lnt=dbnt_lname,
        rnt=dbnt_rname,
        at=address_type,
        lunq=l_uniqueness,
        runq=r_uniqueness,
        rrt=address_type_dict[address_type]
    )
    return rr_dbrt


def dns_record_create(cert, params=None, params_opt=None, ver=DEFAULTVER, pretend=False):
    logger.info("Call dns_record_create(%s,%s) v%s", params, params_opt, ver)
    json_list = list()
    for addrtuple in params:
        host = addrtuple[0]
        for address in addrtuple[1:]:
            data_param_list = [
                {"name": "fqdn", "new_value": host},
                {"name": "inttype", "new_value": _construct_inttype(address)},
                {"name": "data", "new_value": address},
            ]
            for option in params_opt:
                data_param_list.append({"name": option, "new_value": params_opt[option]}, )
            json_list.append({"param_list": data_param_list})

    print(params_opt)
    data = json.dumps(json_list, ensure_ascii=False)
    if not pretend:
        result = _post_name(cert, ver, "dns", "record", "create", data=data)
    else:
        result = data
    return result


def dns_record_delete(cert, params=None, params_opt=None, ver=DEFAULTVER, pretend=False):
    logger.info("Call dns_record_delete(%s,%s) v%s", params, params_opt, ver)
    json_list = list()
    for addrtuple in params:
        host = addrtuple[0]
        for address in addrtuple[1:]:
            data_param_list = [
                {"name": "fqdn", "old_value": host},
                {"name": "inttype", "old_value": _construct_inttype(address)},
                {"name": "data", "old_value": address},
            ]
            for option in params_opt:
                data_param_list.append({"name": option, "new_value": params_opt[option]}, )
            json_list.append({"param_list": data_param_list})

    for option in params_opt:
        params_list = [
            {"name": option, "new_value": params_opt[option]},
        ]
        json_list.append({"param_list": params_list})

    data = json.dumps(json_list, ensure_ascii=False)
    if not pretend:
        result = _post_name(cert, ver, "dns", "record", "delete", data=data)
    else:
        result = data
    return result


def wapi_api(cert, ver=DEFAULTVER):
    logger.info("Call wapi_api v%s", ver)
    data = _query_name(cert, ver, "wapi")
    return data


def wapi_datadict_api(cert, ver=DEFAULTVER):
    logger.info("Call wapi_datadict_api v%s", ver)
    data = _query_name(cert, ver, "wapi", "datadict")
    return data


def wapi_datadict_list(cert, params=None, ver=DEFAULTVER):
    logger.info("Call wapi_datadict_list(%s) v%s", params, ver)
    data = _query_name(cert, ver, "wapi", "datadict", "list", params=params)
    return data


def wapi_datadict_listds(cert, params=None, ver=DEFAULTVER):
    logger.info("Call wapi_datadict_listds(%s) v%s", params, ver)
    data = _query_name(cert, ver, "wapi", "datadict", "list_datastructure", params=params)
    return data
