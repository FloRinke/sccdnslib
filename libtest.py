#!/usr/bin/env python3
# sccdnslib - provide python interface to KIT-SCC's WebAPI
# Copyright (C) 2017  Florian Rinke
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json

from pygments import highlight
from pygments.lexers import javascript
from pygments.formatters import terminal

from sccdnslib import query


def pp_json(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
        print(highlight(json.dumps(json.loads(json_thing),
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    else:
        print(highlight(json.dumps(json_thing,
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    return None


if __name__ == "__main__":
    pp_json(query.get_name("2.0", "dns", "record"))

